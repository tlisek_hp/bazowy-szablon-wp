<!doctype HTML>
<html class="no-js" <?php language_attributes()?>>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <title><?php wp_title()?></title>
    
    <?php wp_head()?>
  </head>
  
  <?php $has_fixed_nav = true;?>
  
  
  <?php $fixed_nav_class = $has_fixed_nav ? 'fixed-nav' : '';?>
  <body <?php body_class($fixed_nav_class)?>>
	  
		<?php get_template_part('partials/general/site-header');?>
		
		<main id="site-wrap">
				