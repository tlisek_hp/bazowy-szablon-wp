<header id="site-header">
	<div class="grid-container">
		<div class="grid-x header-wrap align-justify align-middle">
			<div class="cell logo-wrap shrink">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						Logo
	<!-- 					<img src="assets/img/logo.png"> -->
					</a>
			</div>
			
			<div class="cell menu-wrap shrink">
	      <a id="hamburger" href="#"><span></span></a>
	      
	      <?php if( has_nav_menu( 'main-menu' ) ):?>
	      
				<?php wp_nav_menu(array(
					'theme_location' => 'main-menu',
					'container' => '',
					'menu_class' => 'menu main-menu'
				))?>
				
				<?php else:?>
					<span><?php a_e('Brak przypisanego menu');?></span>
				<?php endif;?>
				
			</div>
					
		</div>
	</div>	    
</header>
