<?php
	
	
// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);

// disable for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);

	
	
// SECURITY
remove_action('wp_head', 'wp_generator');
function my_secure_generator( $generator, $type ) {
	return '';
}
add_filter( 'the_generator', 'my_secure_generator', 10, 2 );

function my_remove_src_version( $src ) {
	global $wp_version;

	$version_str = '?ver='.$wp_version;
	$offset = strlen( $src ) - strlen( $version_str );

	if ( $offset >= 0 && strpos($src, $version_str, $offset) !== FALSE )
		return substr( $src, 0, $offset );

	return $src;
}
add_filter( 'script_loader_src', 'my_remove_src_version' );
add_filter( 'style_loader_src', 'my_remove_src_version' );

add_filter('use_block_editor_for_post', '__return_false', 10);


// ******************** Crunchify Tips - Clean up WordPress Header START ********************** //
function crunchify_remove_version() {return '';}
add_filter('the_generator', 'crunchify_remove_version');
 
remove_action('wp_head', 'rest_output_link_wp_head', 10);
remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
remove_action('template_redirect', 'rest_output_link_header', 11, 0);
 
remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
// ******************** Clean up WordPress Header END ********************** //

add_filter( 'feed_links_show_comments_feed', '__return_false' );

// Remove the REST API endpoint.
remove_action( 'rest_api_init', 'wp_oembed_register_route' );
 
// Turn off oEmbed auto discovery.
add_filter( 'embed_oembed_discover', '__return_false' );
 
// Don't filter oEmbed results.
remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );
 
// Remove oEmbed discovery links.
remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
 
// Remove oEmbed-specific JavaScript from the front-end and back-end.
remove_action( 'wp_head', 'wp_oembed_add_host_js' );
 
// Remove all embeds rewrite rules.
// add_filter( 'rewrite_rules_array', 'disable_embeds_rewrites' );




// Remove emoji
remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
remove_action( 'admin_print_styles', 'print_emoji_styles' );


/*Function to defer or asynchronously load scripts*/
function js_async_attr($tag){
	# Do not add defer or async attribute to these scripts
	
	if( is_admin() ) return $tag;
	
	$scripts_to_exclude = array('jquery', 'contact-form-7');
	
	foreach($scripts_to_exclude as $exclude_script){
		
		if(true == strpos($tag, $exclude_script ) )
		return $tag; 
	}
	
	# Defer or async all remaining scripts not excluded above
	return str_replace( ' src', ' defer="defer" src', $tag );
}
add_filter( 'script_loader_tag', 'js_async_attr', 10 );

//Disable gutenberg style in Front
function wps_deregister_styles() {
  wp_dequeue_style( 'wp-block-library' );
}
add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );


/*
add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );

function enable_cf7_styles() {
	global $post;
	
	if( strpos($post->post_content, '[contact-form-7') ){
		if( function_exists( 'wpcf7_enqueue_scripts' ) ) wpcf7_enqueue_scripts();
		if( function_exists( 'wpcf7_enqueue_styles' ) ) wpcf7_enqueue_styles();
	}
	
}
add_action( 'wp_enqueue_scripts', 'enable_cf7_styles', 100 );
*/
