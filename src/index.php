<?php get_header();?>

<?php global $wp_query;?>

<div class="grid-container">
		
	<div class="grid-x medium-up-3 grid-margin-x grid-margin-y" id="loop">	
		<?php if ( have_posts() ):?>
			<?php while ( have_posts() ): the_post();?>
				<?php get_template_part('partials/post/content-loop');?>
			<?php endwhile;?>
		<?php endif; ?>
	</div>
	
	<?php if($wp_query->max_num_pages > 1):?>
		<div class="text-center">
			<a href="#" class="form-button" data-loadmore data-wrap="#loop" data-template="partials/post/content-loop.php"><span class="text"><?php a_e('Zobacz więcej');?></span><span class="loading"><i class="material-icons">refresh</i></span></a>
		</div>
	<?php endif;?>
	
</div>

<?php get_footer();?>